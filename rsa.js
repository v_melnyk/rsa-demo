function isPrime(value) {
    for(var i = 2; i < value; i++) {
        if(value % i == 0) {
            return false;
        }
    }
    return value > 1;
}

function gen_keys(p, q){
    var n = p * q
    var t = (p - 1) * (q - 1)

    var e = 1

    while (true) {
        if (t % e != 0) {
            if (isPrime(e)) {
                break
            }
        }
        e++;
    }

    var d = 1;

    while (true) {
        if ((d * e) % t != 1) {
            d++;
        } else {
            break;
        }
    }

    return {
        "public": {
            "e": e,
            "n": n
        },
        "private": {
            "d": d,
            "n": n
        }
    }
}



function modexp(a, b, n) {
    a = a % n;
    var result = 1;
    var x = a;

    while(b > 0){
        var leastSignificantBit = b % 2;
        b = Math.floor(b / 2);
        
        if (leastSignificantBit == 1) {
            result = result * x;
            result = result % n;
        }
        x = x * x;
        x = x % n;
    }
    return result;
}

function enc(s, key) {
    var e = key["e"]
    var n = key["n"]
    return s.toString().split('').map(function(c) {
        char_id = c.charCodeAt(0);
        char_id_enc = modexp(char_id, e, n);
        //Math.pow(char_id,e) % n
        return char_id_enc;
    });
}

function dec(a, key) {
    var d = key["d"]
    var n = key["n"]
    // console.log(typeof a);
    return a.map(function(char_id_enc) {
        char_id = modexp(char_id_enc, d, n);
        return char_id;
        }).map(function(c) {
            return String.fromCharCode(c)
    }).join("");
}


// p = 53
// q = 97

// keys = gen_keys(p, q);

// encrypted = enc("Hello", keys["public"]);
// console.log(encrypted);
// console.log(dec(encrypted, keys["private"]));